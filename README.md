Interview Test App
======================


# Running the app

You need to add these environment variables:
`MAPBOX_KEY` : for loading the map from [Mapbox](https://docs.mapbox.com/help/getting-started/access-tokens/)
`FOURSQUARE_SECRET` : for loading venues from [Foursquare](https://developer.foursquare.com/docs/places-api/getting-started/])
`FOURSQUARE_ID` : for loading venues from [Foursquare](https://developer.foursquare.com/docs/places-api/getting-started/])

# Architecture

The project implemented with 
[MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) 
and [The Clean Architecture](https://developer.foursquare.com/docs/places-api/getting-started/])

## Domain
 We have repository and entities here I didn't implement UseCase or Interactor to
 reduce boilerplate codes. Adding UseCases is okay, if we know we have lots of pure
 logic implementation or we want to have some kind of Interactors and share logic
 between different modules but in this case we don't have enough evidence for that.
 The entity keyword has been used as a suffix to describe data classes in domain

**Downside:** using repository interface instead of abstract class cause things like coroutine dispatcher
 and execute function are visible for feature level modules.


### Refactor Strategy:
 
Put all domain level logic in a function when input received by both ViewModel
and repository.
later if we find out we have lots of domain level logic we could add this layer easily.
 
## Data

 We have repository and data source implementations.
 Also we have our models for network with Response and Request suffix.
 Creating different models for data layers cause lots of boilerplate but we can develop tools to 
 generate that automatically. If we don't have time we could use entity models. In my experience
  importing some android related SDKs to domain layer cause other careless import and make testing hard.
   We could also control it with lints and merge request templates for review.
 
 **Note:** I've used retrofit interface as DataSource. We could create another layer here but it's 
 possible we violate YAGNI principle in lots of scenarios.
 
 **Advantage:** we can easily change our data models and change API or migrate to other technologies
 like GRPC. In this scenario we have

### Refactor Strategy:
 
We can set some guidelines in the beginning.
So later if we develop a tool for that we would migrate easily and without change to domain layer entities
 


## Feature Modules

With this strategy we reduce build time in long term and in CI/CD.
We avoid unwanted access to other features class and we can review more effectively. and later if we want have different apps.

**Note:** We should change our dependency direction with the app module
 if we want to have Instant app with Android App Bundle. 
 BTW it seems that it's not hard to refactor this type to that.
 
## Presentation Shared Module

We could implement shared view related classes and themes here.
Also, we could start our design system here before we make a different module for that.


 
## Test Shared Module

It's hard to import test folder with gradle. In this way we can handle it and also with `api` 
keyword for shared SDKs in test gradle, we avoid some boilerplate as well.

# Technologies And Decisions

In this section I'll try to explain reason behind some of my decisions

## Hilt

We Use hilt because of less boilerplate codes. Also, generally I think it's easier for integration tests as well.

For ViewModel injection there's alpha library from google
 that I'm not sure if it's stable or not.
 So I've used some boilerplate codes but we can easily refactor to use that library

**Create Hilt modules in the related gradle modules:**
This scenario could help us, if we want to reuse our gradle modules in different apps or 
if we don't want to mock them in some integration tests.

**Create Hilt modules in the aoo module:**
This scenario is easier to test for component testing but it needs more boilerplate codes. 

PS: It's my first time using hilt in project


## Coroutine and Flow

For the threading and observing I could use technologies like Livedata with Threadpools, Rx and Coroutine.

It's hard to handle things like back pressure
or debounce with Livedatas and ThreadPools.

On the other hand, Coroutine is lighter than Rx. It's native and somehow
it's easier to test because Google has created some libraries for that. 

PS: It's my first time using coroutine and related testing SDKs in a project.
I tried to read and use some codes from Google samples.

## Mapbox

I wanted to test Google map for this project, but there are sanctions and I couldn't use it. 
So I've imported mapbox because I had experience using mapbox.

## Performance optimization

The Foursquare search API doesn't support pagination, also it does not support
ViewPort scenario and I tried to get the best I can from that.
Because of the requirements I implemented clustering for points on map.
Also I used data structures like TreeMap in InMemoryDataSource to optimize searching.

If we were able to change how backend interact to client, we would get better result:

* We could use datasource tiles from mapbox and add cluster point on different zoom level

* We could pre calculate number venues that we want to show Using algorithms like
 [R-tree](https://en.wikipedia.org/wiki/R-tree) and
 [K-d tree](https://en.wikipedia.org/wiki/K-d_tree)
 to mimic tile like behavior and reduce the number and size of the requests


## UX

I keep UI minimal and tried to show I know how to use things like constraint layout, styles, dimens drawables, etc, but I know from the user side it's not good at all.

Also, I know best practices like showing why you want a permission.


**Update:** After deadline, I've XMLs for supporting landscape.
[There is problem with ViewBinding](https://issuetracker.google.com/issues/143683987)
 approach so I had to add view hierarchy which is not good for performance. We can use DataBinding instead. both of them have some pros and cons of course.


## Test

I wrote 2 test for one Data source and one view model as examples.

I tried to use stubs. [Stubs do better in terms of state verification](https://martinfowler.com/articles/mocksArentStubs.html)
, but I added Mockk for Repository test and show how we can Leverage behavior verification tools in a good way.
