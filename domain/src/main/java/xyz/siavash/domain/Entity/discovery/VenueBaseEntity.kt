package xyz.siavash.domain.entity.discovery

data class VenueBaseEntity(
  val id: String,
  val name: String,
  val locationEntity: LocationEntity
)
