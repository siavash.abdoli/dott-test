package xyz.siavash.domain.entity.discovery

data class VenuesDataEntity(
  val venueBases: List<VenueBaseEntity>,
  val isContinuationOfItems: Boolean
)
