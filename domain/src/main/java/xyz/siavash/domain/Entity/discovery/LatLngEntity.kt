package xyz.siavash.domain.entity.discovery

data class LatLngEntity(
  val latitude: Double,
  val longitude: Double,
  val zoom: Double = 12.5
)
