package xyz.siavash.domain.entity.discovery

data class LatLngBoundEntity(
  val bottomLat: Double,
  val topLat: Double,
  val leftLong: Double,
  val rightLong: Double
)
