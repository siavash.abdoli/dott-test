package xyz.siavash.domain.entity.discovery

data class LocationEntity(
  val lat: Double,
  val lng: Double
)
