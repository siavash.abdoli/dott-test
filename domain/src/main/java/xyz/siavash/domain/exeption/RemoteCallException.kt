package xyz.siavash.domain.exeption

import java.lang.Exception

class RemoteCallException(override val message: String?) : Exception(message)
