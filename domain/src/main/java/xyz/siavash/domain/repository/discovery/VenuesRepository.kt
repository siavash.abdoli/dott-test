package xyz.siavash.domain.repository.discovery

import xyz.siavash.domain.FlowRepository
import xyz.siavash.domain.entities.discovery.LatLngBoundEntity
import xyz.siavash.domain.entities.discovery.VenuesDataEntity

interface VenuesRepository : FlowRepository<LatLngBoundEntity, VenuesDataEntity>
