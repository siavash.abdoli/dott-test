package xyz.siavash.domain.repository.discovery

import xyz.siavash.domain.Repository
import xyz.siavash.domain.entities.discovery.VenueDetailEntity

interface VenueDetailRepository : Repository<String, VenueDetailEntity>
