package xyz.siavash.domain.entities.discovery

data class LocationEntity(
  val lat: Double,
  val lng: Double
)
