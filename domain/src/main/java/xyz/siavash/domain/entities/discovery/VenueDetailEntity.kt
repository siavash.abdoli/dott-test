package xyz.siavash.domain.entities.discovery

data class VenueDetailEntity(
  val id: String,
  val name: String,
  val mainImage: String?,
  val websiteURL: String?,
  val phoneNumberFormatted: String?,
  val phoneNumber: String?,
  val address: String?
)
