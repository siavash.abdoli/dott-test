package xyz.siavash.domain.entities.discovery

data class LatLngBoundEntity(
  val bottomLat: Double,
  val topLat: Double,
  val leftLong: Double,
  val rightLong: Double
)
