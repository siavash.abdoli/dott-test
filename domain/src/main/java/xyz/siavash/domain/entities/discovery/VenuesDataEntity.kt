package xyz.siavash.domain.entities.discovery

data class VenuesDataEntity(
    val venueBases: List<VenueBaseEntity>,
    val isContinuationOfItems: Boolean
)
