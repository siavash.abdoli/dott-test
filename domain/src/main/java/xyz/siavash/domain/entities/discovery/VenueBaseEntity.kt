package xyz.siavash.domain.entities.discovery

data class VenueBaseEntity(
  val id: String,
  val name: String,
  val locationEntity: LocationEntity
)
