package xyz.siavash.domain.entities.discovery

data class LatLngEntity(
  val latitude: Double,
  val longitude: Double,
  val zoom: Double = 12.5
)
