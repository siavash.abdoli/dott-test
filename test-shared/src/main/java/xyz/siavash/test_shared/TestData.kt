package xyz.siavash.test_shared

import xyz.siavash.domain.entities.discovery.LocationEntity
import xyz.siavash.domain.entities.discovery.VenueBaseEntity

object TestData {
  val list = mutableListOf<VenueBaseEntity>(
    // in lat long bound of 3 to 10 for lat and 100 to 110 for long
    VenueBaseEntity("1001", "title 1", LocationEntity(3.5, 103.5)),
    VenueBaseEntity("1002", "title 2", LocationEntity(4.35, 104.5224)),
    VenueBaseEntity("1003", "title 3", LocationEntity(7.5, 108.25)),
    VenueBaseEntity("1004", "title 4", LocationEntity(3.3, 109.75)),
    VenueBaseEntity("1005", "title 5", LocationEntity(8.2, 101.502)),
    VenueBaseEntity("1006", "title 6", LocationEntity(5.12, 104.34)),
    VenueBaseEntity("1007", "title 7", LocationEntity(9.3, 109.23)),
    VenueBaseEntity("1008", "title 8", LocationEntity(4.98, 106.91)),
    VenueBaseEntity("1009", "title 9", LocationEntity(8.23, 104.5)),
    VenueBaseEntity("1010", "title 10", LocationEntity(4.5, 104.5)),
    // in lat long bound of 7 to 10 for lat and 105 to 110 for long
    VenueBaseEntity("1011", "title 11", LocationEntity(7.93, 105.73)),
    VenueBaseEntity("1012", "title 12", LocationEntity(9.25, 109.32)),
    VenueBaseEntity("1013", "title 13", LocationEntity(8.25, 109.46)),
    VenueBaseEntity("1014", "title 14", LocationEntity(9.23, 108.73)),
    VenueBaseEntity("1015", "title 15", LocationEntity(7.23, 108.83)),
    // in lat long bound of 10 to 14 for lat and 110 to 115 for long
    VenueBaseEntity("1016", "title 16", LocationEntity(13.26, 111.57)),
    VenueBaseEntity("1017", "title 17", LocationEntity(12.26, 114.37)),
    VenueBaseEntity("1018", "title 18", LocationEntity(10.26, 112.37)),
    VenueBaseEntity("1019", "title 19", LocationEntity(11.26, 111.47)),
    // out of scope
    VenueBaseEntity("1020", "title 20", LocationEntity(10.26, 105.47)),
    VenueBaseEntity("1021", "title 21", LocationEntity(12.26, 109.47)),
    VenueBaseEntity("1022", "title 22", LocationEntity(7.26, 111.47)),
    VenueBaseEntity("1023", "title 1", LocationEntity(8.26, 113.47)),
  )
}
