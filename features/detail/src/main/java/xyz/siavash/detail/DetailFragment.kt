package xyz.siavash.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import xyz.siavash.detail.databinding.DetailFragmentBinding
import xyz.siavash.presentation_shared.base.DottBaseFragment

@AndroidEntryPoint
class DetailFragment : DottBaseFragment() {

  companion object {
    private const val VENUE_ID = "id"

    fun newInstance(venueId: String): DetailFragment {
      val fragment = DetailFragment().apply {
        arguments = Bundle().apply {
          putString(VENUE_ID, venueId)
        }
      }
      return fragment
    }
  }

  private var _binding: DetailFragmentBinding? = null

  // using livedata and lifecycle to avoid crash with binding so it should be fine
  // it's okay to crash if someone try to access it in other state
  private val binding get() = _binding!!

  private val viewModel: DetailViewModel by getLazyViewModel()

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    _binding = DetailFragmentBinding.inflate(inflater, container, false)
    val view = binding.root
    return view
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    viewModel.setVenueId(requireArguments().getString(VENUE_ID)!!)
    initObservers()
  }

  private fun initObservers() {
    viewModel.updateData.observe(
      viewLifecycleOwner,
      Observer {
        binding.venueContent.detailTitle.text = it.name
        binding.venueContent.detailAddress.text = it.address
        binding.venueContent.detailWebsite.visibility = it.websiteVisibility()
        binding.venueContent.detailWebsiteText.text = it.websiteURL
        binding.venueContent.detailPhone.visibility = it.phoneVisibility()
        binding.venueContent.detailPhoneText.text = it.getPhoneForShow()

        binding.detailImage.visibility = if (it.isImageVisible()) {
          Glide.with(binding.detailImage)
            .load(it.mainImage)
            .centerCrop()
            .into(binding.detailImage)
          View.VISIBLE
        } else {
          View.GONE
        }
      }
    )
  }

  override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
  }
}
