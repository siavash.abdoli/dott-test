package xyz.siavash.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import xyz.siavash.presentation_shared.base.Event
import kotlinx.coroutines.launch
import xyz.siavash.domain.entities.Result
import xyz.siavash.domain.entities.discovery.VenueDetailEntity
import xyz.siavash.domain.repository.discovery.VenueDetailRepository
import xyz.siavash.presentation_shared.base.TextData
import javax.inject.Inject

class DetailViewModel @Inject constructor(val venueDetailRepository: VenueDetailRepository) : ViewModel() {
  private lateinit var id: String
  private var needToRequestForId = true

  private val _showError = MutableLiveData<Event<TextData>>()
  val showError: LiveData<Event<TextData>> = _showError

  private val _updateData = MutableLiveData<VenueDetailEntity>()
  val updateData: LiveData<VenueDetailEntity> = _updateData

  fun setVenueId(id: String) {
    if (needToRequestForId.not()) {
      return
    }
    needToRequestForId = false
    this.id = id
    viewModelScope.launch {
      val result = venueDetailRepository(id)
      when (result) {
        is Result.Success -> {
          // the !! mark lead to warning but, I have to add it. because when I remove, the color turns to red!
          _updateData.value = result.data!!
        }
        is Result.Error -> {
          needToRequestForId = true
        }
        else -> {
          needToRequestForId = true
        }
      }
    }
  }
}
