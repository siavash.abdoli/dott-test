package xyz.siavash.detail

import android.view.View
import xyz.siavash.domain.entities.discovery.VenueDetailEntity

fun VenueDetailEntity.websiteVisibility(): Int {
  return if (websiteURL != null) {
    View.VISIBLE
  } else {
    View.GONE
  }
}

fun VenueDetailEntity.phoneVisibility(): Int {
  return if (phoneNumber != null || phoneNumberFormatted != null) {
    View.VISIBLE
  } else {
    View.GONE
  }
}

fun VenueDetailEntity.isImageVisible(): Boolean {
  return mainImage != null && mainImage != "original"
}

fun VenueDetailEntity.getPhoneForShow(): String? {
  return if (phoneNumberFormatted != null) {
    phoneNumberFormatted
  } else {
    phoneNumber
  }
}
