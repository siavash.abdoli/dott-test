package xyz.siavash.map.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import kotlinx.coroutines.delay
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import xyz.siavash.domain.entities.discovery.VenuesDataEntity
import xyz.siavash.map.main.util.FakeVenuesRepository
import xyz.siavash.test_shared.LiveDataTestUtil
import xyz.siavash.test_shared.MainCoroutineRule
import xyz.siavash.test_shared.TestData
import xyz.siavash.test_shared.runBlockingTest

class HomeViewModelTest {

  @get:Rule
  var coroutineRule = MainCoroutineRule()

  @get:Rule
  var instantTaskExecutorRule = InstantTaskExecutorRule()

  lateinit var viewModel: HomeViewModel

  lateinit var repository: FakeVenuesRepository

  @Before
  fun setup() {
    repository = FakeVenuesRepository(coroutineRule.testDispatcher)
    viewModel = HomeViewModel(repository)
  }

  @Test
  fun checkCacheAndRemoteWorkProperly() = coroutineRule.runBlockingTest {
    // when
    val boundingBox = LatLngBounds.from(1.2, 1.2, 1.2, 1.2)
    val firstEmit = VenuesDataEntity(TestData.list.subList(0, 10), false)
    val secondEmit = VenuesDataEntity(TestData.list.subList(5, 15), true)
    val lastEmit = VenuesDataEntity(TestData.list.subList(0, 15), false)
    repository.setEmits(firstEmit, secondEmit)
    val feature1 = firstEmit.toFeatureCollection(mutableListOf())
    val feature2 = lastEmit.toFeatureCollection(mutableListOf())

    // given
    val results = LiveDataTestUtil.getValuesLater(viewModel.showVenues)
    viewModel.viewPortChanged(boundingBox)
    delay(510)

    // then
    Assert.assertEquals(results[0]!!.features()!!, feature1.features())
    Assert.assertEquals(results[1]!!.features()!!, feature2.features())
  }

  @Test
  fun checkContinuationOfItemsWorkProperly() = coroutineRule.runBlockingTest {
    // when
    val boundingBox = LatLngBounds.from(1.2, 1.2, 1.2, 1.2)
    val firstEmit = VenuesDataEntity(TestData.list.subList(0, 10), false)
    val secondEmit = VenuesDataEntity(TestData.list.subList(3, 14), false)
    repository.setEmits(firstEmit, secondEmit)
    val feature1 = firstEmit.toFeatureCollection(mutableListOf())
    val feature2 = secondEmit.toFeatureCollection(mutableListOf())

    // given
    val results = LiveDataTestUtil.getValuesLater(viewModel.showVenues)
    viewModel.viewPortChanged(boundingBox)
    delay(510)

    // then
    Assert.assertEquals(results[0]!!.features()!!, feature1.features())
    Assert.assertEquals(results[1]!!.features()!!, feature2.features())
  }

  @Test
  fun checkCancellationWorkProperly() = coroutineRule.runBlockingTest {
    // when
    val boundingBox = LatLngBounds.from(1.2, 1.2, 1.2, 1.2)
    val firstEmit = VenuesDataEntity(TestData.list.subList(0, 10), false)
    val secondEmit = VenuesDataEntity(TestData.list.subList(3, 14), true)
    repository.setEmits(firstEmit, secondEmit)

    // given
    val results = LiveDataTestUtil.getValuesLater(viewModel.showVenues)
    viewModel.viewPortChanged(boundingBox)
    viewModel.viewPortChanged(boundingBox)
    delay(600)

    // then
    Assert.assertEquals(results.size, 2)
  }
}
