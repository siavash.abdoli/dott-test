package xyz.siavash.map.main.util

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import xyz.siavash.domain.entities.Result
import xyz.siavash.domain.entities.discovery.LatLngBoundEntity
import xyz.siavash.domain.entities.discovery.VenuesDataEntity
import xyz.siavash.domain.repository.discovery.VenuesRepository

class FakeVenuesRepository(override val coroutineDispatcher: CoroutineDispatcher) : VenuesRepository {
  private lateinit var firstEmit: VenuesDataEntity
  private lateinit var secondEmit: VenuesDataEntity

  fun setEmits(firstEmit: VenuesDataEntity, secondEmit: VenuesDataEntity) {
    this.firstEmit = firstEmit
    this.secondEmit = secondEmit
  }
  override suspend fun execute(parameters: LatLngBoundEntity): Flow<Result<VenuesDataEntity>> {
    return flow {
      emit(Result.Success(firstEmit))
      emit(Result.Success(secondEmit))
    }
  }
}
