package xyz.siavash.map.main

import android.content.Context
import android.graphics.Color
import android.graphics.RectF
import androidx.core.content.ContextCompat
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.expressions.Expression
import com.mapbox.mapboxsdk.style.layers.CircleLayer
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.utils.BitmapUtils
import xyz.siavash.map.R
import xyz.siavash.presentation_shared.base.getDimen
import java.net.URISyntaxException

class VenuesClusterHandler(
  private val context: Context,
  private val mapboxMap: MapboxMap,
  style: Style,
  venueClicked: (String) -> Unit
) {

  private val VENUE_ID: String = "venuesource"
  private val POINT_COUNT: String = "point_count"
  private val VENUE_ICON_ID: String = "venue-icon-id"
  private var sourceInitialized: Boolean = false
  private lateinit var source: GeoJsonSource
  private val venueClickArea: Int = context.getDimen(R.dimen.map_clickable_area_size)

  init {
    style.addImage(
      VENUE_ICON_ID,
      BitmapUtils.getBitmapFromDrawable(ContextCompat.getDrawable(context, R.drawable.venues_icon_drawble))!!,
      true
    )
    mapboxMap.addOnMapClickListener { clickedLatLng: LatLng ->
      val clickedRect = createVenueClickRect(clickedLatLng)
      val foundFeature = findFeaturesInArea(clickedRect, "unclustered-points")
      val venueId = foundFeature?.properties()?.get("venue-id")?.asString
      if (venueId != null) {
        venueClicked(venueId)
        return@addOnMapClickListener true
      }
      return@addOnMapClickListener false
    }
  }

  private fun findFeaturesInArea(clickedRect: RectF, layerId: String): Feature? {
    return mapboxMap.queryRenderedFeatures(clickedRect, layerId).firstOrNull()
  }

  private fun createVenueClickRect(clickedLatLng: LatLng): RectF {
    val pixel = mapboxMap.projection.toScreenLocation(clickedLatLng)
    return RectF(pixel.x - venueClickArea, pixel.y - venueClickArea, pixel.x + venueClickArea, pixel.y + venueClickArea)
  }

  fun addClusteredGeoJsonSource(loadedMapStyle: Style, featureCollection: FeatureCollection) {
    if (sourceInitialized) {
      source.setGeoJson(featureCollection)
      return
    }
    source = GeoJsonSource(
      VENUE_ID,
      featureCollection,
      GeoJsonOptions()
        .withCluster(true)
        .withClusterMaxZoom(14)
        .withClusterRadius(80)
    )
    sourceInitialized = true
    // Add a new source from the GeoJSON data and set the 'cluster' option to true.
    try {
      loadedMapStyle.addSource(source)
    } catch (uriSyntaxException: URISyntaxException) {
    }

// Creating a marker layer for single data points
    val unclustered = SymbolLayer("unclustered-points", VENUE_ID)
    unclustered.setProperties(
      PropertyFactory.iconImage(VENUE_ICON_ID),
      PropertyFactory.iconSize(
        Expression.literal(0.5f)

      )
    )
    unclustered.setFilter(Expression.has("venue-id"))
    loadedMapStyle.addLayer(unclustered)

// Use the Venue GeoJSON source to create four layers: One layer for each cluster category.
// Each point range gets a different fill color.
    val layers = arrayOf(
      intArrayOf(200, ContextCompat.getColor(context, R.color.color_high_count)),
      intArrayOf(30, ContextCompat.getColor(context, R.color.color_medium_count)),
      intArrayOf(5, ContextCompat.getColor(context, R.color.color_low_count)),
      intArrayOf(0, ContextCompat.getColor(context, R.color.color_very_low_count))
    )
    for (i in layers.indices) {
// Add clusters' circles
      val circles = CircleLayer("cluster-$i", VENUE_ID)
      circles.setProperties(
        PropertyFactory.circleColor(layers[i][1]),
        PropertyFactory.circleRadius(18f)
      )
      val pointCount = Expression.toNumber(Expression.get(POINT_COUNT))

// Add a filter to the cluster layer that hides the circles based on POINT_COUNT
      circles.setFilter(
        if (i == 0) Expression.all(
          Expression.has(POINT_COUNT),
          Expression.gte(
            pointCount,
            Expression.literal(
              layers[i][0]
            )
          )
        ) else Expression.all(
          Expression.has(POINT_COUNT),
          Expression.gte(
            pointCount,
            Expression.literal(
              layers[i][0]
            )
          ),
          Expression.lt(
            pointCount,
            Expression.literal(
              layers[i - 1][0]
            )
          )
        )
      )
      loadedMapStyle.addLayer(circles)
    }

    // Add the count labels
    val count = SymbolLayer("count", VENUE_ID)
    count.setProperties(
      PropertyFactory.textField(Expression.toString(Expression.get(POINT_COUNT))),
      PropertyFactory.textSize(12f),
      PropertyFactory.textColor(Color.WHITE),
      PropertyFactory.textIgnorePlacement(true),
      PropertyFactory.textAllowOverlap(true)
    )
    loadedMapStyle.addLayer(count)
  }
}
