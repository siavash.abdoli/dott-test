package xyz.siavash.map.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.MapboxMapOptions
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.TransitionOptions
import com.mapbox.mapboxsdk.utils.MapFragmentUtils
import dagger.hilt.android.AndroidEntryPoint
import xyz.siavash.domain.entities.discovery.LatLngEntity
import xyz.siavash.map.R
import xyz.siavash.presentation_shared.base.AppPage
import xyz.siavash.presentation_shared.base.DottBaseFragment
import xyz.siavash.presentation_shared.base.shortToast

@AndroidEntryPoint
class HomeFragment : DottBaseFragment(), OnMapReadyCallback {

  var mapboxMap: MapboxMap? = null
  lateinit var venuesClusterHandler: VenuesClusterHandler

  // using field here because accessing view model early cause problem with injection
  // it's good Idea to write UI test for this part
  private var cameraMoveLocation: LatLng? = null
  private var zoom: Double = 12.5

  companion object {
    val LATITUDE = "latitude"
    val LONGITUDE = "longitude"
    val ZOOM = "zoom"
    fun newInstance(context: Context, latLngEntity: LatLngEntity?): HomeFragment {
      val mapFragment = HomeFragment()
      val mapboxMapOptions = MapboxMapOptions.createFromAttributes(context)
      val bundle = MapFragmentUtils.createFragmentArgs(mapboxMapOptions)
      // doing this because we only want to move first time so using bundle is unnecessary
      mapFragment.cameraMoveLocation = LatLng(
        latLngEntity?.latitude ?: 52.345364,
        latLngEntity?.longitude ?: 4.8629696
      )

      mapFragment.arguments = bundle
      return mapFragment
    }
  }

  private lateinit var root: View
  val viewModel: HomeViewModel by getLazyViewModel()

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    root = inflater.inflate(R.layout.home_fragment, container, false)
    return root
  }

  private val STYLES = arrayOf<String>(
    Style.LIGHT,
  )

  private var map: MapView? = null

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    if (savedInstanceState != null) {
      val latitude = savedInstanceState.getDouble(LATITUDE)
      val longitude = savedInstanceState.getDouble(LONGITUDE)
      zoom = savedInstanceState.getDouble(ZOOM)
      cameraMoveLocation = LatLng(latitude, longitude)
    }
    cameraMoveLocation?.let {
      viewModel.savedCameraLocation(
        LatLngEntity(it.latitude, it.longitude, zoom)
      )
    }
    map = root.findViewById(R.id.map_view)
    map!!.onCreate(savedInstanceState)
    map!!.getMapAsync(this)
  }

  override fun onMapReady(mapboxMap: MapboxMap) {
    this.mapboxMap = mapboxMap
    mapboxMap.addOnCameraIdleListener {
      viewModel.viewPortChanged(getBundleCameraBounds(mapboxMap))
    }

    mapboxMap.setStyle(
      Style.Builder().fromUri(
        STYLES[0]
      )
    ) { style ->
      venuesClusterHandler = VenuesClusterHandler(requireContext(), mapboxMap, style, ::onVenueClicked)

      // Disable any type of fading transition when icons collide on the map. This enhances the visual
      // look of the data clustering together and breaking apart.
      style.transition = TransitionOptions(0, 0, false)


      initObservers(style)
    }
  }

  private fun onVenueClicked(venueId: String) {
    appNavigator.navigateTo(AppPage.DetailPage(venueId))
  }

  private fun initObservers(style: Style) {
    viewModel.showVenues.observe(
      viewLifecycleOwner,
      Observer {
        venuesClusterHandler.addClusteredGeoJsonSource(style, it)
      }
    )
    viewModel.showError.observe(
      viewLifecycleOwner,
      Observer { event ->
        event.getContentIfNotHandled()?.let { textData ->
          textData.shortToast(requireContext())
        }
      }
    )
    viewModel.moveCamera.observe(
      viewLifecycleOwner,
      Observer { event ->
        cameraMoveLocation?.let {
          event.getContentIfNotHandled()?.let {
            mapboxMap?.animateCamera(
              CameraUpdateFactory.newLatLngZoom(
                LatLng(it.latitude, it.longitude),
                it.zoom
              )
            )
          }
        }
      }
    )
  }

  override fun onStart() {
    super.onStart()
    map?.onStart()
  }

  override fun onResume() {
    super.onResume()
    map?.onResume()
  }

  override fun onPause() {
    super.onPause()
    map?.onPause()
  }

  override fun onStop() {
    super.onStop()
    map?.onStop()
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)
    map?.onSaveInstanceState(outState)
    mapboxMap?.let {
      outState.putDouble(LATITUDE, it.cameraPosition.target.latitude)
      outState.putDouble(LONGITUDE, it.cameraPosition.target.longitude)
      outState.putDouble(ZOOM, it.cameraPosition.zoom)
    }
  }

  override fun onLowMemory() {
    super.onLowMemory()
    map?.onLowMemory()
  }

  override fun onDestroyView() {
    super.onDestroyView()
    mapboxMap?.let {
      val target = it.cameraPosition.target
      viewModel.savedCameraLocation(LatLngEntity(target.latitude, target.longitude, it.cameraPosition.zoom))
    }
    map?.onDestroy()
    mapboxMap = null
  }

  private fun getBundleCameraBounds(mapbox: MapboxMap): LatLngBounds {
    return mapbox.projection.getVisibleRegion(true).latLngBounds
  }
}
