package xyz.siavash.map.main

import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import xyz.siavash.domain.entities.discovery.LatLngBoundEntity
import xyz.siavash.domain.entities.discovery.VenueBaseEntity
import xyz.siavash.domain.entities.discovery.VenuesDataEntity
import java.util.TreeSet
import kotlin.Comparator

fun VenueBaseEntity.toMapFeaturePoint(): Feature {
  val point = Point.fromLngLat(this.locationEntity.lng, this.locationEntity.lat)
  val jsonObject = JsonObject()
  jsonObject.addProperty("venue-name", this.name)
  jsonObject.addProperty("venue-id", this.id)
  return Feature.fromGeometry(point, jsonObject)
}

fun VenuesDataEntity.toFeatureCollection(features: MutableList<Feature>): FeatureCollection {
  return FeatureCollection.fromFeatures(this.venueBases.toFeature(features))
}

private fun List<VenueBaseEntity>.toFeature(features: MutableList<Feature>): List<Feature> {
  val list = TreeSet(PointComparator())
  list.addAll(features)
  for (venue in this) {
    list.add(venue.toMapFeaturePoint())
  }
  return list.toList()
}

fun LatLngBounds.toEntity(): LatLngBoundEntity {
  return LatLngBoundEntity(
    bottomLat = this.latSouth,
    topLat = this.latNorth,
    leftLong = this.lonWest,
    rightLong = this.lonEast
  )
}

class PointComparator : Comparator<Feature> {
  override fun compare(o1: Feature, o2: Feature): Int {
    return o1.getStringProperty("venue-id").compareTo(o2.getStringProperty("venue-id"))
  }
}
