package xyz.siavash.map.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import xyz.siavash.presentation_shared.base.Event
import com.mapbox.geojson.FeatureCollection
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import xyz.siavash.domain.entities.Result
import xyz.siavash.domain.entities.discovery.LatLngEntity
import xyz.siavash.domain.entities.discovery.VenuesDataEntity
import xyz.siavash.domain.repository.discovery.VenuesRepository
import xyz.siavash.presentation_shared.base.TextData
import xyz.siavash.presentation_shared.base.parseErrorStringRes
import javax.inject.Inject

class HomeViewModel @Inject constructor(val venuesRepository: VenuesRepository) : ViewModel() {

  private var aggregatedResult: FeatureCollection? = null
  private var searchJob: Job? = null

  private val _showVenues = MutableLiveData<FeatureCollection>()
  val showVenues: LiveData<FeatureCollection> = _showVenues

  private val _showError = MutableLiveData<Event<TextData>>()
  val showError: LiveData<Event<TextData>> = _showError

  private val _moveCamera = MutableLiveData<Event<LatLngEntity>>()
  val moveCamera: LiveData<Event<LatLngEntity>> = _moveCamera

  fun viewPortChanged(boundingBox: LatLngBounds) {
    searchJob?.cancel()

    searchJob = viewModelScope.launch {
      // debounce to avoid too many requests
      delay(500)
      venuesRepository(boundingBox.toEntity())
        .collect { showResults(it) }
    }
  }

  private fun showResults(venuesDataResults: Result<VenuesDataEntity>) {
    when (venuesDataResults) {
      is Result.Success -> {
        val venuesData = venuesDataResults.data
        val aggregatedFeatures =
          if (venuesData.isContinuationOfItems && aggregatedResult?.features() != null) {
            aggregatedResult!!.features()!!
          } else {
            mutableListOf()
          }
        aggregatedResult = venuesData.toFeatureCollection(aggregatedFeatures)
        _showVenues.value = aggregatedResult
      }
      Result.Loading -> {
      }
      is Result.Error -> {
        venuesDataResults.exception.let {
          _showError.value = Event(it.parseErrorStringRes())
        }
      }
    }
  }

  fun savedCameraLocation(cameraMoveLocation: LatLngEntity?) {
    cameraMoveLocation?.let {
      _moveCamera.value = Event(it)
    }
  }
}
