package xyz.siavash.data.discovery

import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import xyz.siavash.domain.entities.discovery.LatLngBoundEntity
import xyz.siavash.test_shared.MainCoroutineRule
import xyz.siavash.test_shared.TestData

class VenuesInMemoryDataSourceTest {
  lateinit var venuesInMemoryDataSource: VenuesInMemoryDataSource
  val list = TestData.list

  @get:Rule
  var coroutineRule = MainCoroutineRule()

  @Before
  fun setup() {
    venuesInMemoryDataSource = VenuesInMemoryDataSource()
  }

  @Test
  fun checkVenuesExistInCache() = runBlocking {
    // when
    val bound = LatLngBoundEntity(0.0, 20.0, 100.0, 200.0)
    val data = list.subList(0, 10)

    // action
    venuesInMemoryDataSource.addVenues(data)
    val result = venuesInMemoryDataSource.getMatchingVenues(bound).sortedBy { it.id }

    // then
    Assert.assertEquals(data, result)
  }

  @Test
  fun checkPartialDataCacheWorks() = runBlocking {
    val expected = list.subList(15, 19)
    val data = list
    val bound = LatLngBoundEntity(10.0, 15.0, 110.0, 115.0)

    venuesInMemoryDataSource.addVenues(data)
    val result = venuesInMemoryDataSource.getMatchingVenues(bound).sortedBy { it.id }

    Assert.assertEquals(expected, result)
  }

  @Test
  fun checkLastOperationHasNoSideEffect() = runBlocking {
    val expected = list.subList(15, 19)
    val data = list
    val bound = LatLngBoundEntity(10.0, 15.0, 110.0, 115.0)
    val bound2 = LatLngBoundEntity(0.0, 20.0, 100.0, 120.0)

    venuesInMemoryDataSource.addVenues(data)
    venuesInMemoryDataSource.getMatchingVenues(bound)
    val result = venuesInMemoryDataSource.getMatchingVenues(bound2).sortedBy { it.id }

    Assert.assertEquals(data, result)
  }
}
