package xyz.siavash.data.discovery

import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.internalSubstitute
import junit.framework.TestCase
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response
import xyz.siavash.data.models.discovery.map.FourSquareResponse
import xyz.siavash.data.models.discovery.map.VenuesSearchResponse
import xyz.siavash.test_shared.MainCoroutineRule
import xyz.siavash.test_shared.TestData
import xyz.siavash.data.discovery.util.NetworkTestData
import xyz.siavash.domain.entities.Result
import xyz.siavash.domain.entities.discovery.LatLngBoundEntity
import xyz.siavash.domain.entities.discovery.VenuesDataEntity

class VenuesRepositoryImplTest {

  lateinit var venuesRepositoryImpl: VenuesRepositoryImpl

  @MockK
  lateinit var venuesDataSource: VenuesDataSource

  @MockK
  lateinit var remoteResult: Response<FourSquareResponse<VenuesSearchResponse>>

  @MockK
  lateinit var remoteResponseResult: FourSquareResponse<VenuesSearchResponse>

  // Using real object: read more about it on README.md Test section
  val venuesLocalDataSource = VenuesInMemoryDataSource()

  val list = TestData.list

  @get:Rule
  var coroutineRule = MainCoroutineRule()

  @Before
  fun setup() {
    MockKAnnotations.init(this, relaxUnitFun = true, relaxed = true)
    venuesRepositoryImpl = VenuesRepositoryImpl(
      venuesDataSource,
      venuesLocalDataSource,
      coroutineRule.testDispatcher,
      coroutineRule.testDispatcher
    )
  }

  @Test
  fun checkRepositoryEmitBothLocalAndNetwork() = runBlockingTest {
    venuesLocalDataSource.addVenues(list.subList(0, 10))
    val expectedLocal = VenuesDataEntity(list.subList(0, 10), false)
    val expectedRemote = VenuesDataEntity(list.subList(5, 16), true)

    coEvery { venuesDataSource.searchInArea(ll = any(), radius = any()) } returns remoteResult
    every { remoteResult.isSuccessful } returns true
    every { remoteResult.body() } returns remoteResponseResult
    every { remoteResponseResult.response } returns NetworkTestData.remoteList(5, 16)
    val bounding = LatLngBoundEntity(3.0, 10.0, 100.0, 110.0)


    val results: List<Result<VenuesDataEntity>> = venuesRepositoryImpl.execute(bounding).toList()

    Assert.assertEquals(2, results.size)
    Assert.assertEquals(expectedLocal.venueBases, (results[0] as Result.Success).data.venueBases.sortedBy { it.id })
    Assert.assertEquals(expectedRemote.venueBases, (results[1] as Result.Success).data.venueBases.sortedBy { it.id })
    Assert.assertEquals(expectedLocal.isContinuationOfItems, (results[0] as Result.Success).data.isContinuationOfItems)
    Assert.assertEquals(expectedRemote.isContinuationOfItems, (results[1] as Result.Success).data.isContinuationOfItems)
  }

  @Test
  fun checkRepositoryEmitCachedData() = runBlockingTest {
    val expectedLocal = VenuesDataEntity(list.subList(0, 10), false)

    coEvery { venuesDataSource.searchInArea(ll = any(), radius = any()) } returns remoteResult
    every { remoteResult.isSuccessful } returns true
    every { remoteResult.body() } returns remoteResponseResult
    every { remoteResponseResult.response } returns NetworkTestData.remoteList(0, 10)
    val bounding = LatLngBoundEntity(3.0, 10.0, 100.0, 110.0)

    venuesRepositoryImpl.execute(bounding).toList()
    val results: List<Result<VenuesDataEntity>> = venuesRepositoryImpl.execute(bounding).toList()

    Assert.assertEquals(expectedLocal.venueBases, (results[0] as Result.Success).data.venueBases.sortedBy { it.id })
    Assert.assertEquals(expectedLocal.isContinuationOfItems, (results[0] as Result.Success).data.isContinuationOfItems)
  }
}
