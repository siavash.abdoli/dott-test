package xyz.siavash.data.discovery.util

import xyz.siavash.data.models.discovery.map.Location
import xyz.siavash.data.models.discovery.map.Venues
import xyz.siavash.data.models.discovery.map.VenuesSearchResponse
import xyz.siavash.domain.entities.discovery.VenueBaseEntity
import xyz.siavash.test_shared.TestData

object NetworkTestData {
  fun remoteList(fromIndex: Int, toIndex: Int): VenuesSearchResponse {
    return VenuesSearchResponse(TestData.list.toResponseList(fromIndex, toIndex))
  }


}

private fun List<VenueBaseEntity>.toResponseList(fromIndex: Int, toIndex: Int): MutableList<Venues> {
  val result = mutableListOf<Venues>()
  for (index in fromIndex until toIndex) {
    result.add(this[index].toResponse())
  }
  return result
}

private fun VenueBaseEntity.toResponse(): Venues {
  return Venues(
    id = id,
    name = name,
    location = Location(
      lat = locationEntity.lat,
      lng = locationEntity.lng
    )
  )
}
