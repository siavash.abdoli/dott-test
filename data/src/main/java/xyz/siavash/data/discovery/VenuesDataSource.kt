package xyz.siavash.data.discovery

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import xyz.siavash.data.BuildConfig
import xyz.siavash.data.models.discovery.detail.VenueDetailResponse
import xyz.siavash.data.models.discovery.map.FourSquareResponse
import xyz.siavash.data.models.discovery.map.VenuesSearchResponse

interface VenuesDataSource {
  companion object {
    private const val PAGE_SIZE = 50
  }

  @GET("venues/search")
  suspend fun searchInArea(
    @Query("ll") ll: String,
    @Query("radius") radius: Int,
    @Query("limit") limit: Int = PAGE_SIZE,
    @Query("client_id") clientId: String = BuildConfig.CLIENT_ID,
    @Query("client_secret") clientSecret: String = BuildConfig.CLINET_SECRET,
    @Query("v") apiVersion: Int = 20210820,
    @Query("categoryId") categoryId: String = "4d4b7105d754a06374d81259"
  ): Response<FourSquareResponse<VenuesSearchResponse>>

  @GET("venues/{venueId}")
  suspend fun venueDetail(
    @Path("venueId") venueId: String,
    @Query("client_id") clientId: String = BuildConfig.CLIENT_ID,
    @Query("client_secret") clientSecret: String = BuildConfig.CLINET_SECRET,
    @Query("v") apiVersion: Int = 20210820,
  ): Response<FourSquareResponse<VenueDetailResponse>>
}
