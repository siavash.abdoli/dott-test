package xyz.siavash.data.discovery

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import xyz.siavash.domain.entities.discovery.LatLngBoundEntity
import xyz.siavash.domain.entities.discovery.VenueBaseEntity
import java.util.TreeMap
import javax.inject.Inject
import kotlin.Comparator
import kotlin.collections.HashMap

interface VenuesLocalDataSource {
  suspend fun getMatchingVenues(latLngBoundEntity: LatLngBoundEntity): List<VenueBaseEntity>
  suspend fun addVenues(venues: List<VenueBaseEntity>)
}

class VenuesInMemoryDataSource @Inject constructor() : VenuesLocalDataSource {
  private val latitudeMap = TreeMap<Pair<Double, Int>, String>(LocationComparator())
  private val longitudeMap = TreeMap<Pair<Double, Int>, String>(LocationComparator())
  private val venuesIdMap = TreeMap<String, VenueBaseEntity>()

  // count added to handle duplicates in lat and long
  @Volatile
  private var count = 0

  override suspend fun getMatchingVenues(latLngBound: LatLngBoundEntity): List<VenueBaseEntity> {
    val resultIds =
      latitudeMap.subMap(latLngBound.bottomLat to -1, latLngBound.topLat to count + 1).values.toMutableSet()
    val includedLngIds = longitudeMap.subMap(latLngBound.leftLong to -1, latLngBound.rightLong to count + 1).values
    resultIds.retainAll(includedLngIds)
    val copy = HashMap(venuesIdMap)
    copy.keys.retainAll(resultIds)
    return copy.values.toList()
  }

  override suspend fun addVenues(venues: List<VenueBaseEntity>) {
    for (venue in venues) {
      if (!venuesIdMap.containsKey(venue.id)) {
        venuesIdMap[venue.id] = venue
        if (latitudeMap.containsKey(venue.locationEntity.lat to count) ||
          longitudeMap.containsKey(venue.locationEntity.lng to count)
        ) {
          count++
        }
        latitudeMap[venue.locationEntity.lat to count] = venue.id
        longitudeMap[venue.locationEntity.lng to count] = venue.id
      }
    }
  }
}

private class LocationComparator : Comparator<Pair<Double, Int>> {
  override fun compare(o1: Pair<Double, Int>, o2: Pair<Double, Int>): Int {
    if (o1.first == o2.first) {
      return o1.second.compareTo(o2.second)
    }
    return o1.first.compareTo(o2.first)
  }
}

@Module
@InstallIn(SingletonComponent::class)
interface VenuesDataSourceModule {

  @Binds
  fun bindVenuesDataSource(venuesInMemoryDataSource: VenuesInMemoryDataSource): VenuesLocalDataSource
}
