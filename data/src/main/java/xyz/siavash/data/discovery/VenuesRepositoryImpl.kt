package xyz.siavash.data.discovery

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import xyz.siavash.data.models.discovery.map.FourSquareResponse
import xyz.siavash.data.models.discovery.map.VenuesSearchResponse
import xyz.siavash.data.models.discovery.toVenuesBaseEntity
import xyz.siavash.domain.DefaultDispatcher
import xyz.siavash.domain.IoDispatcher
import xyz.siavash.domain.entities.Result
import xyz.siavash.domain.entities.discovery.LatLngBoundEntity
import xyz.siavash.domain.entities.discovery.VenuesDataEntity
import xyz.siavash.domain.exeption.RemoteCallException
import xyz.siavash.domain.repository.discovery.VenuesRepository
import javax.inject.Inject
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin
import kotlin.math.sqrt

/**
 * I use different interface for local and remote because of accessibility and optimization in how we call it but
 * for lots of other scenarios it's better to have one interface to apply Liskov substitution principle
 *
 */
class VenuesRepositoryImpl @Inject constructor(
  private val remote: VenuesDataSource,
  private val local: VenuesLocalDataSource,
  @DefaultDispatcher override val coroutineDispatcher: CoroutineDispatcher,
  @IoDispatcher val ioDispatcher: CoroutineDispatcher
) : VenuesRepository {

  override suspend fun execute(bound: LatLngBoundEntity): Flow<Result<VenuesDataEntity>> {
    var remoteResponse: Response<FourSquareResponse<VenuesSearchResponse>>? = null
    return flow {
      // start async request so we could use network while we loading from cache
      val remoteDeferred = CoroutineScope(ioDispatcher).async {
        remoteResponse = remote.searchInArea(bound.getCenterLocation(), bound.getRadiusInMeter())
      }

      // read and emit from local
      val cachedVenues = local.getMatchingVenues(bound)
      val earlyResult = VenuesDataEntity(cachedVenues, false)
      if (earlyResult.venueBases.isNotEmpty()) {
        emit(Result.Success(earlyResult))
      }

      // wait for remote call to complete and emit result
      remoteDeferred.await()
      if (remoteResponse?.isSuccessful == false) {
        emit(Result.Error(RemoteCallException(remoteResponse!!.errorBody()?.string())))
        return@flow
      }
      val remoteVenues = remoteResponse?.body()?.response?.venues?.toVenuesBaseEntity()
      remoteVenues?.let {
        val result = VenuesDataEntity(it, true)
        emit(Result.Success(result))
        local.addVenues(it)
      } ?: emit(Result.Error(RemoteCallException("Venues are null")))
    }
  }
}

private fun LatLngBoundEntity.getRadiusInMeter(): Int {
  val horizontalDistance = distanceBetweenToPointInMeter(bottomLat, leftLong, bottomLat, rightLong)
  val verticalDistance = distanceBetweenToPointInMeter(bottomLat, leftLong, topLat, leftLong)
  return min(horizontalDistance, verticalDistance) / 2
}

private fun LatLngBoundEntity.getCenterLocation(): String {
  return "${(topLat + bottomLat) / 2},${(rightLong + leftLong) / 2}"
}

// It's not an accurate way but I assume we later want to change our API to work in bounding box way
fun distanceBetweenToPointInMeter(latA: Double, longA: Double, latB: Double, longB: Double): Int {
  val earthRadiusInMiles = 3958.75
  val latDiff = Math.toRadians((latB - latA))
  val lngDiff = Math.toRadians((longB - longA))
  val a = sin(latDiff / 2) * sin(latDiff / 2) +
    cos(Math.toRadians(latA)) * cos(Math.toRadians(latB)) *
    sin(lngDiff / 2) * sin(lngDiff / 2)
  val c = 2 * atan2(sqrt(a), sqrt(1 - a))
  val distance = earthRadiusInMiles * c
  val meterConversion = 1609
  return (distance * meterConversion).toInt()
}
