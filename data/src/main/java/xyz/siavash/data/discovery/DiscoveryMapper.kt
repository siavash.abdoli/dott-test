package xyz.siavash.data.models.discovery

import xyz.siavash.data.models.discovery.detail.VenueDetailResponse
import xyz.siavash.data.models.discovery.map.FourSquareResponse
import xyz.siavash.data.models.discovery.map.Location
import xyz.siavash.data.models.discovery.map.Venues
import xyz.siavash.domain.entities.discovery.LocationEntity
import xyz.siavash.domain.entities.discovery.VenueBaseEntity
import xyz.siavash.domain.entities.discovery.VenueDetailEntity

fun Venues.toVenueBaseEntity(): VenueBaseEntity {
  return VenueBaseEntity(
    id = this.id,
    name = this.name,
    locationEntity = this.location.toLocationEntity()
  )
}

private fun Location.toLocationEntity(): LocationEntity {
  return LocationEntity(
    lat = this.lat,
    lng = this.lng
  )
}

fun List<Venues>.toVenuesBaseEntity(): List<VenueBaseEntity> {
  val result = mutableListOf<VenueBaseEntity>()
  for (venue in this) {
    result.add(venue.toVenueBaseEntity())
  }
  return result
}

fun FourSquareResponse<VenueDetailResponse>.toEntity(): VenueDetailEntity {
  return VenueDetailEntity(
    id = response.venue!!.id!!,
    name = response.venue.name!!,
    mainImage = response.venue.bestPhoto?.prefix + "original" + response.venue.bestPhoto?.suffix,
    phoneNumber = response.venue.contact?.phone,
    phoneNumberFormatted = response.venue.contact?.formattedPhone,
    websiteURL = response.venue.url,
    address = response.venue.location?.address
  )
}
