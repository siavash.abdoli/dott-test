package xyz.siavash.data.discovery

import kotlinx.coroutines.CoroutineDispatcher
import xyz.siavash.data.models.discovery.toEntity
import xyz.siavash.domain.IoDispatcher
import xyz.siavash.domain.entities.discovery.VenueDetailEntity
import xyz.siavash.domain.repository.discovery.VenueDetailRepository
import javax.inject.Inject

class VenueDetailRepositoryImpl @Inject constructor(
  private val venuesDataSource: VenuesDataSource,
  @IoDispatcher override val coroutineDispatcher: CoroutineDispatcher
) : VenueDetailRepository {
  override suspend fun execute(id: String): VenueDetailEntity {
    val response = venuesDataSource.venueDetail(venueId = id)
    return response.body()!!.toEntity()
  }
}
