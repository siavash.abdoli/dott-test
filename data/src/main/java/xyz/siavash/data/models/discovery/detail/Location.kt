package xyz.siavash.data.models.discovery.detail

import com.squareup.moshi.Json

data class Location(
  @Json(name = "address")
  val address: String?,
  @Json(name = "city")
  val city: String?,
  @Json(name = "country")
  val country: String?,
  @Json(name = "formattedAddress")
  val formattedAddress: List<String>?
)
