package xyz.siavash.data.models.discovery.map

import com.squareup.moshi.Json

data class FourSquareResponse<T>(
  @Json(name = "meta") val meta: Meta,
  @Json(name = "response") val response: T
)
