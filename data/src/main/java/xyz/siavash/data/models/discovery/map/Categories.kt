package xyz.siavash.data.models.discovery.map

import com.squareup.moshi.Json

data class Categories(
  @Json(name = "id") val id: String?,
  @Json(name = "name") val name: String?,
  @Json(name = "pluralName") val pluralName: String?,
  @Json(name = "shortName") val shortName: String?,
  @Json(name = "icon") val icon: Icon?,
  @Json(name = "primary") val primary: Boolean?
)
