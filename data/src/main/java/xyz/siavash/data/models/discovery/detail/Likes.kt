package xyz.siavash.data.models.discovery.detail

import com.squareup.moshi.Json

data class Likes(
  @Json(name = "count")
  val count: Int?
)
