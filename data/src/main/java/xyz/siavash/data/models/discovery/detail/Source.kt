package xyz.siavash.data.models.discovery.detail

import com.squareup.moshi.Json

data class Source(
  @Json(name = "name")
  val name: String?,
  @Json(name = "url")
  val url: String?
)
