package xyz.siavash.data.models.discovery.detail

import com.squareup.moshi.Json

data class VenueDetailResponse(
  @Json(name = "venue")
  val venue: VenueDetail?
)
