package xyz.siavash.data.models.discovery.map

import com.squareup.moshi.Json

data class Venues(
  @Json(name = "id") val id: String,
  @Json(name = "name") val name: String,
  @Json(name = "location") val location: Location
)
