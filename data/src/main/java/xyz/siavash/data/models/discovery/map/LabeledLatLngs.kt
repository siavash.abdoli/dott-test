package xyz.siavash.data.models.discovery.map

import com.squareup.moshi.Json

data class LabeledLatLngs(
  @Json(name = "label") val label: String,
  @Json(name = "lat") val lat: Double,
  @Json(name = "lng") val lng: Double
)
