package xyz.siavash.data.models.discovery.map

import com.squareup.moshi.Json

data class VenuesSearchResponse(
  @Json(name = "venues") val venues: List<Venues>
)
