package xyz.siavash.data.models.discovery.detail

import com.squareup.moshi.Json

data class VenueDetail(
  @Json(name = "bestPhoto")
  val bestPhoto: BestPhoto?,
  @Json(name = "contact")
  val contact: Contact?,
  @Json(name = "id")
  val id: String?,
  @Json(name = "rating")
  val rating: Float?,
  @Json(name = "url")
  val url: String?,
  @Json(name = "location")
  val location: Location?,
  @Json(name = "name")
  val name: String?
)
