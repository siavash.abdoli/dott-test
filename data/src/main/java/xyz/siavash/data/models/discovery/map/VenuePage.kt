package xyz.siavash.data.models.discovery.map

import com.squareup.moshi.Json

data class VenuePage(
  @Json(name = "id") val id: Int
)
