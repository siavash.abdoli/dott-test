package xyz.siavash.data.models.discovery.map

import com.squareup.moshi.Json

data class Icon(
  @Json(name = "prefix") val prefix: String,
  @Json(name = "suffix") val suffix: String
)
