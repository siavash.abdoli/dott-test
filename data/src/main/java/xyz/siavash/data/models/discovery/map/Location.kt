package xyz.siavash.data.models.discovery.map

import com.squareup.moshi.Json

data class Location(
  @Json(name = "lat") val lat: Double,
  @Json(name = "lng") val lng: Double,
)
