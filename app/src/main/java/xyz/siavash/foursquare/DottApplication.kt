
package xyz.siavash.foursquare

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DottApplication : MultiDexApplication()
