package xyz.siavash.foursquare.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import xyz.siavash.data.BuildConfig
import xyz.siavash.data.discovery.VenuesDataSource

@InstallIn(SingletonComponent::class)
@Module
class VenuesApiModule {

  @Provides
  fun provideRetrofitService(): VenuesDataSource {
    val builder = OkHttpClient.Builder()
    if (BuildConfig.DEBUG) {
      val interceptor = HttpLoggingInterceptor()
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
      builder.addInterceptor(interceptor)
    }
    val client = builder.build()
    return Retrofit.Builder()
      .baseUrl("https://api.foursquare.com/v2/")
      .addConverterFactory(
        MoshiConverterFactory.create(
          Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        )
      )
      .client(client)
      .build()
      .create(VenuesDataSource::class.java)
  }
}
