package xyz.siavash.foursquare.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import xyz.siavash.data.discovery.VenueDetailRepositoryImpl
import xyz.siavash.data.discovery.VenuesRepositoryImpl
import xyz.siavash.domain.repository.discovery.VenueDetailRepository
import xyz.siavash.domain.repository.discovery.VenuesRepository

@InstallIn(SingletonComponent::class)
@Module
class DataModules {

  @Provides
  fun provideVenuesRepository(venuesRepositoryImpl: VenuesRepositoryImpl): VenuesRepository {
    return venuesRepositoryImpl
  }

  @Provides
  fun provideVenueDetailRepository(repository: VenueDetailRepositoryImpl): VenueDetailRepository {
    return repository
  }
}
