package xyz.siavash.foursquare.di

import android.content.Context
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import xyz.siavash.presentation_shared.base.AppNavigator

@InstallIn(ActivityComponent::class)
@Module
class ActivityModule {

  @Provides
  fun provideAppNavigator(dottAppNavigator: DottAppNavigator): AppNavigator {
    return dottAppNavigator
  }

  @Provides
  fun provideLocationClient(@ActivityContext context: Context): FusedLocationProviderClient {
    return LocationServices.getFusedLocationProviderClient(context)
  }
}
