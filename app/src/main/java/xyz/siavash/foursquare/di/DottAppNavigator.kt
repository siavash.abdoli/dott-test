package xyz.siavash.foursquare.di

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.qualifiers.ActivityContext
import xyz.siavash.detail.DetailFragment
import xyz.siavash.foursquare.R
import xyz.siavash.map.main.HomeFragment
import xyz.siavash.presentation_shared.base.AppNavigator
import xyz.siavash.presentation_shared.base.AppPage
import javax.inject.Inject

class DottAppNavigator @Inject constructor(@ActivityContext private val activity: Context) : AppNavigator {
  override fun navigateTo(appPage: AppPage) {
    when (appPage) {
      is AppPage.DetailPage -> {
        (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
          .replace(R.id.container, DetailFragment.newInstance(appPage.venueId))
          .addToBackStack("DetailPage")
          .commit()
      }
      is AppPage.HomePage -> {
        (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
          .replace(R.id.container, HomeFragment.newInstance(activity, appPage.latLngEntity))
          .commit()
      }
    }
  }
}
