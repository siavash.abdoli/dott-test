package xyz.siavash.foursquare

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.tasks.Task
import com.mapbox.mapboxsdk.Mapbox
import dagger.hilt.android.AndroidEntryPoint
import xyz.siavash.presentation_shared.base.AppNavigator
import xyz.siavash.presentation_shared.base.AppPage
import xyz.siavash.presentation_shared.base.TextData
import xyz.siavash.presentation_shared.base.shortToast
import xyz.siavash.presentation_shared.base.toLatLngEntity
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

  private var task: Task<Location>? = null
  val REQUEST_LOCATION_PERMISSION: Int = 501
  val REQUEST_PHONE_PERMISSION: Int = 502

  @Inject
  lateinit var appNavigator: AppNavigator

  @Inject
  lateinit var fusedLocationClient: FusedLocationProviderClient

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.main_activity)
    Mapbox.getInstance(
      this,
      BuildConfig.MAPBOX_KEY
    )

    if (savedInstanceState != null) {
      return
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R &&
      ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.READ_PHONE_STATE
      ) != PackageManager.PERMISSION_GRANTED
    ) {
      requestPermissions(
        arrayOf(Manifest.permission.READ_PHONE_STATE),
        REQUEST_PHONE_PERMISSION
      )
      return
    }
    if (checkLocationPermissionAndRequest().not()) {
      return
    }

    getLocationAndNavigate()
  }

  @SuppressLint("MissingPermission")
  private fun getLocationAndNavigate() {
    // In some rare situations this can be null but I do it for sake of simplicity
    fusedLocationClient.lastLocation
      .addOnSuccessListener { location: Location? ->
        appNavigator.navigateTo(AppPage.HomePage(location?.toLatLngEntity()))
        Log.d("SIAVASHHHHHHHHHHHH", "successsssss")
      }
      .addOnFailureListener {
        appNavigator.navigateTo(AppPage.HomePage(null))
        Log.d("SIAVASHHHHHHHHHHHH", "failure")
      }
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    if (requestCode == REQUEST_LOCATION_PERMISSION) {
      if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        getLocationAndNavigate()
      } else {
        TextData.TextStringRes(R.string.need_permission).shortToast(this)
      }
    } else if (requestCode == REQUEST_PHONE_PERMISSION) {
      if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        checkLocationPermissionAndRequest()
      } else {
        TextData.TextStringRes(R.string.need_permission).shortToast(this)
      }
    }
  }

  private fun checkLocationPermissionAndRequest(): Boolean {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
      ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.ACCESS_FINE_LOCATION
      ) != PackageManager.PERMISSION_GRANTED &&
      ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.ACCESS_COARSE_LOCATION
      ) != PackageManager.PERMISSION_GRANTED
    ) {
      requestPermissions(
        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
        REQUEST_LOCATION_PERMISSION
      )
      return false
    }
    return true
  }
}
