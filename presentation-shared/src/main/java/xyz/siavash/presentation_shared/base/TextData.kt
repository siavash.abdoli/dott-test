package xyz.siavash.presentation_shared.base

sealed class TextData {
  class TextStringRes(val resId: Int) : TextData()
  class TextString(val text: String) : TextData()
}
