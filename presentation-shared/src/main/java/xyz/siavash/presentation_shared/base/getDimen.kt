package xyz.siavash.presentation_shared.base

import android.content.Context
import androidx.annotation.DimenRes

fun Context.getDimen(@DimenRes resId: Int) = resources.getDimensionPixelSize(resId)
