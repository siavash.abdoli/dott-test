package xyz.siavash.presentation_shared.base

import android.content.Context
import android.location.Location
import android.widget.Toast
import xyz.siavash.domain.entities.discovery.LatLngEntity
import xyz.siavash.domain.exeption.RemoteCallException
import xyz.siavash.presentation_shared.R

fun Throwable.parseErrorStringRes(): TextData {
  return when (this) {
    is RemoteCallException -> {
      TextData.TextStringRes(R.string.remote_error)
    }
    else -> {
      TextData.TextStringRes(R.string.unkown_error)
    }
  }
}

fun TextData.shortToast(requireContext: Context) {
  when (this) {
    is TextData.TextString -> Toast.makeText(requireContext, this.text, Toast.LENGTH_SHORT).show()
    is TextData.TextStringRes -> Toast.makeText(requireContext, this.resId, Toast.LENGTH_SHORT).show()
  }
}

fun Location.toLatLngEntity(): LatLngEntity {
  return LatLngEntity(
    latitude = this.latitude,
    longitude = this.longitude
  )
}
