package xyz.siavash.presentation_shared.base

import xyz.siavash.domain.entities.discovery.LatLngEntity

interface AppNavigator {
  fun navigateTo(appPage: AppPage)
}

sealed class AppPage {
  class HomePage(val latLngEntity: LatLngEntity?) : AppPage()
  class DetailPage(val venueId: String) : AppPage()
}
